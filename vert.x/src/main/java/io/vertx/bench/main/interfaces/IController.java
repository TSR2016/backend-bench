package io.vertx.bench.main.interfaces;

public interface IController {
	String showFirstPageData();
	String showRandomPageData();
}