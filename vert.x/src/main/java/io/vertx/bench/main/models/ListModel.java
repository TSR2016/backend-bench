package io.vertx.bench.main.models;

import io.vertx.bench.main.models.Row;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.Random;
import java.util.*;
import org.apache.commons.pool.ObjectPool;

import io.vertx.bench.main.interfaces.IModel;

import io.vertx.core.logging.Logger;

public class ListModel implements IModel {
	private ObjectPool _mysql = null;
	private Logger _log;
	
	public ListModel(ObjectPool mysql, Logger log) {
		this._mysql = mysql;
		this._log = log;
	}
	
	private ArrayList<Row> _execSelect(String sql) {
		ArrayList<Row> list = new ArrayList<Row>(50);
		Connection conn = null;
		Statement st = null;
		ResultSet res = null;
		try {
			conn = (Connection)this._mysql.borrowObject();
			st = conn.createStatement();
			res = st.executeQuery(sql);
			while(res.next()) {
				list.add(new Row(res.getInt(1), res.getString(2), res.getFloat(3)));
			}
			this._mysql.returnObject(conn);
		} catch (SQLException e) {
			this._log.error("SQLException in ListModel: " + e.getMessage() + ", stack: " + e.getStackTrace());
		} catch (Exception e) {
			this._log.error("Exception in ListModel: " + e.getMessage() + ", stack: " + e.getStackTrace());
		} finally {
			safeClose(res);
			safeClose(st);
			safeClose(conn);
		}
		return list;
	} 
	
	public ArrayList<Row> getFirstPageData() {
		return this._execSelect("select * from bench_data where id <= 50");
	}
	
	public ArrayList<Row> getRandomPageData() {
		Random randomGenerator = new Random();
		StringBuilder sql = new StringBuilder("select * from bench_data where id in (");
		sql.ensureCapacity(sql.capacity() + 50 * (5 + 1) + 1);
		for (int i = 0; i < 50; i++) {
			sql.append(randomGenerator.nextInt((10000 - 1) + 1 ) + 1);
			sql.append(',');
		}
		sql.setLength(sql.length() - 1);
		sql.append(')');
		return this._execSelect(sql.toString());
	}
	
	private void safeClose(Connection conn) {
        if (conn != null) {
            try {
                 _mysql.returnObject(conn);
            } catch (Exception e) {
               	this._log.error("Exception in ListModel: " + e.getMessage() + ", stack: " + e.getStackTrace());
			}
        }
    }
 
     private void safeClose(ResultSet res) {
        if (res != null) {
            try {
                res.close();
            } catch (SQLException e) {
               	this._log.error("SQLException in ListModel: " + e.getMessage() + ", stack: " + e.getStackTrace());
			}
        }
     }
 
     private void safeClose(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
               	this._log.error("SQLException in ListModel: " + e.getMessage() + ", stack: " + e.getStackTrace());
			}
     	}
     }
}