package io.vertx.bench.main.services;

import java.sql.DriverManager;
import org.apache.commons.pool.BasePoolableObjectFactory;

import io.vertx.core.logging.Logger;

public class MysqlAdapter extends BasePoolableObjectFactory {
	private String host;
	private int port;
    private String schema;
    private String user;
    private String password;
	 
	public MysqlAdapter(String host, String schema, String user, String password) {
		this.host = host;
		this.port = 3306;
		this.schema = schema;
		this.user = user;
		this.password = password;												
	}
	
	@Override
	public Object makeObject() throws Exception {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		String url = "jdbc:mysql://" + host + ":" + port + "/"
			+ schema + "?autoReconnectForPools=true";
		
		return DriverManager.getConnection(url, user, password);
	}
}