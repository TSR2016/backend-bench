package io.vertx.bench.main.models;

public class Row {	
	public int id;
	public String name;
	public float price;
	
	Row(int id, String name, float price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}
}