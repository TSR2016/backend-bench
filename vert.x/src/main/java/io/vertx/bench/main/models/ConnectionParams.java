package io.vertx.bench.main.models;

public class ConnectionParams {	
	public String host;
	public String dbName;
	public String user;
	public String pass;
}