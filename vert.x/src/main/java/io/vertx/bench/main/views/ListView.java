package io.vertx.bench.main.views;

import io.vertx.core.json.Json;
import io.vertx.bench.main.models.Row;
import io.vertx.bench.main.interfaces.IView;
import java.util.*;

public class ListView implements IView {
	public String renderJson(ArrayList<Row> data) {
		return Json.encodePrettily(data);	
	}
}