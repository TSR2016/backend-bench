package io.vertx.bench.main.interfaces;

import io.vertx.bench.main.models.Row;
import java.util.*;

public interface IView {
	String renderJson(ArrayList<Row> data);
}