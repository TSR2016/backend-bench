package io.vertx.bench.main.controllers;

import io.vertx.bench.main.interfaces.IController;
import io.vertx.bench.main.interfaces.IModel;
import io.vertx.bench.main.interfaces.IView;
import java.util.*;

public class ListController implements IController {
	private	IModel _model;
	private IView _view;

	public ListController(IModel model, IView view) { 
		_model = model;
		_view = view;	
	}
	
	public String showFirstPageData() {
		ArrayList data = new ArrayList();
		data = _model.getFirstPageData();
		return _view.renderJson(data);
	}
	
	public String showRandomPageData() {
		ArrayList data = new ArrayList();
		data = _model.getRandomPageData();
		return _view.renderJson(data);
	}
};