package io.vertx.bench.main.interfaces;

import io.vertx.bench.main.models.Row;
import java.util.*;

public interface IModel {
	ArrayList<Row> getFirstPageData();
	ArrayList<Row> getRandomPageData();
}