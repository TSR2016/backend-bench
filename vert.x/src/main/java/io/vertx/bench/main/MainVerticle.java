package io.vertx.bench.main;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.bench.main.models.ConnectionParams;

import io.vertx.bench.main.views.ListView;
import io.vertx.bench.main.services.MysqlAdapter;
import io.vertx.bench.main.services.SettingsService;
import io.vertx.bench.main.models.ListModel;
import io.vertx.bench.main.controllers.ListController;

import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.commons.pool.impl.GenericObjectPoolFactory;
import org.apache.commons.pool.impl.GenericObjectPool.Config;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.net.URLDecoder;
import java.io.*;

public class MainVerticle extends AbstractVerticle {
    private ConnectionParams params = null;
    private ObjectPool mysql_pool = null;
    private Logger log = null;
    
    private void helloHandler(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        response
            .putHeader("content-type", "text/html")
            .end("hello");
    }
    
    private void firstPageHandler(RoutingContext routingContext) {
        ListView view = new ListView();
        ListModel model = new ListModel(this.mysql_pool, this.log);
        ListController controller = new ListController(model, view);
        String reply = controller.showFirstPageData();
        HttpServerResponse response = routingContext.response();
        response
            .putHeader("content-type", "application/json")
            .end(reply);
    }
    
    private void randomHandler(RoutingContext routingContext) {
        ListView view = new ListView();
        ListModel model = new ListModel(this.mysql_pool, this.log);
        ListController controller = new ListController(model, view);
        String reply = controller.showRandomPageData();
        HttpServerResponse response = routingContext.response();
        response
            .putHeader("content-type", "application/json")
            .end(reply);
    }
    
    @Override
    public void start(Future<Void> fut) {
        try {
            this.log = LoggerFactory.getLogger(MainVerticle.class);
            SettingsService settings = new SettingsService(this.log);
            StringBuilder path = new StringBuilder();
            String basepath = MainVerticle.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            String decodedPath = URLDecoder.decode(basepath, "UTF-8");
            File f = new File(decodedPath);
            path.append(f.getAbsoluteFile().getParentFile().toString());
            path.append("/../../configDb.json");
            this.params = settings.parseSettings(path.toString());
            MysqlAdapter mysql_pool_factory = new MysqlAdapter(this.params.host, this.params.dbName, this.params.user, this.params.pass);
            Config config = new GenericObjectPool.Config();
                config.maxActive = 100;
                config.testOnBorrow = true;
                config.testWhileIdle = true;
                config.timeBetweenEvictionRunsMillis = 10000;
                config.minEvictableIdleTimeMillis = 60000;
    
            GenericObjectPoolFactory genericObjectPoolFactory = new GenericObjectPoolFactory(mysql_pool_factory, config);
            this.mysql_pool = genericObjectPoolFactory.createPool();
            
            Router router = Router.router(vertx);
            
            router.get("/hello").handler(this::helloHandler);
            router.get("/first_page_list").handler(this::firstPageHandler);
            router.get("/random_list").handler(this::randomHandler);
            
            vertx
                .createHttpServer()
                .requestHandler(router::accept)
                .listen(2345, result -> {
                    if (result.succeeded()) {
                    fut.complete();
                    } else {
                    fut.fail(result.cause());
                    }
                });
        } catch(java.io.UnsupportedEncodingException e) {
            
        }
    }
}