package io.vertx.bench.main.services;

import io.vertx.core.json.Json;
import io.vertx.bench.main.models.Row;
import io.vertx.bench.main.models.ConnectionParams;

import java.io.*;

import io.vertx.core.logging.Logger;

public class SettingsService {
    private Logger _log = null;
    public SettingsService(Logger log) {
        this._log = log;
    }
    
	private String getFileContent(String filePath) {
        try {
            File file = new File(filePath);
            byte bt[] = new byte[(int)file.length()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(bt);
            fis.close();

            return new String(bt, "ISO-8859-1");
        }
        catch(FileNotFoundException e){
            this._log.error("getFileContent:" + e.getMessage());
        }
        catch(IOException e){
            this._log.error("getFileContent:" + e.getMessage());
        }
        catch(Exception e){
            this._log.error("getFileContent:" + e.getMessage());
        }

        return "";
    }
	
	public ConnectionParams parseSettings(String filePath) {
		return Json.decodeValue(getFileContent(filePath), ConnectionParams.class);
	}
}