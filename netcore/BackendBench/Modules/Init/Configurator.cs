using BenchApp.Modules.StaticText.Controllers;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;

namespace BenchApp.Modules.Init;
public class Configurator
{
    public static void ParseEnv() {
        var root = Directory.GetCurrentDirectory();
        var dotenv = Path.Combine(root, ".env");
        DotEnv.Load(dotenv);
    }

    public static void ConfigureMysqlConnector(WebApplicationBuilder builder) {
        string ConfString = string.Format("Server=127.0.0.1;Database={0};Uid={1};Pwd={2};",
            Environment.GetEnvironmentVariable("DOTNET_DB_DATABASE"),
            Environment.GetEnvironmentVariable("DOTNET_DB_USER"),
            Environment.GetEnvironmentVariable("DOTNET_DB_PASSWORD")
        );
        builder.Services.AddMySqlDataSource(ConfString);
    }
};
