using System;
using BenchApp.Modules.List.Models;
using BenchApp.Modules.List.Repositories;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;

namespace BenchApp.Modules.List.Controllers
{
    [Route("/")]
    public class BenchDataController : ControllerBase
    {
        [HttpGet("first_page_data")]
        public async  Task<List<BenchDataModel>> FirstPageData([FromServices] MySqlDataSource db)
        {
            var repository = new BenchDataRepository(db);
            return await repository.FirstPageData();
        }

        [HttpGet("random_data")]
        public async  Task<List<BenchDataModel>> RandomPageData([FromServices] MySqlDataSource db)
        {
            var repository = new BenchDataRepository(db);
            return await repository.RandomPageData();
        }
    }
}