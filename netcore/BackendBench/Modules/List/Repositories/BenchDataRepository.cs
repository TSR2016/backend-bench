using System.Data.Common;
using MySqlConnector;
using BenchApp.Modules.List.Models;

namespace BenchApp.Modules.List.Repositories;

public class BenchDataRepository(MySqlDataSource database)
{
    public async Task<List<BenchDataModel>> FirstPageData()
    {
        using var connection = await database.OpenConnectionAsync();
        using var command = connection.CreateCommand();
        command.CommandText = @"SELECT `id`, `name`, `price` FROM `bench_data` limit 50";
        var result = await ReadAllAsync(await command.ExecuteReaderAsync());
        return result;
    }

    public async Task<List<BenchDataModel>> RandomPageData()
    {
        using var connection = await database.OpenConnectionAsync();
        using var command = connection.CreateCommand();
        Random rand = new Random();
        var ints = Enumerable.Range(0, 50)
                                     .Select(i => rand.Next(1, 10000));
        var where = string.Join(",", ints);
        command.CommandText = @"SELECT `id`, `name`, `price` FROM `bench_data` WHERE id in (" + where + ")";
        var result = await ReadAllAsync(await command.ExecuteReaderAsync());
        return result;
    }

    private async Task<List<BenchDataModel>> ReadAllAsync(DbDataReader reader)
    {
        var data = new List<BenchDataModel>();
        using (reader)
        {
            while (await reader.ReadAsync())
            {
                var record = new BenchDataModel
                {
                    Id = reader.GetInt32(0),
                    Name = reader.GetString(1),
                    Price = reader.GetFloat(2),
                };
                data.Add(record);
            }
        }
        return data;
    }
}