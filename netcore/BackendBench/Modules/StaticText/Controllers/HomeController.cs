using System;
using Microsoft.AspNetCore.Mvc;

namespace BenchApp.Modules.StaticText.Controllers
{
    [Route("/")]
    public class HomeController : ControllerBase
    {
        [HttpGet("/")]
        public ActionResult Home()
        {
            return Redirect("hello");
        }
    }
}