using System;
using Microsoft.AspNetCore.Mvc;

namespace BenchApp.Modules.StaticText.Controllers
{
    [Route("/")]
    public class HelloController : ControllerBase
    {
        [HttpGet("hello")]
        public ActionResult Get()
        {
            return Content("Hello World!");
        }
    }
}