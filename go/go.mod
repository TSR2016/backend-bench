module backend-bench/go

go 1.14

require (
	github.com/caarlos0/env/v6 v6.10.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.1
	github.com/joho/godotenv v1.5.1
	github.com/valyala/fasthttp v1.12.0
	gopkg.in/tylerb/graceful.v1 v1.2.15
)
