package repositories

import (
	"math/rand"
	"strconv"
	"strings"

	"backend-bench/go/internal/interfaces"
	"backend-bench/go/internal/models"

	_ "github.com/go-sql-driver/mysql"
)

type ListModel struct {
	m_mysql interfaces.MysqlAdapter
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func CreateListModel(mysql interfaces.MysqlAdapter) *ListModel {
	model := ListModel{m_mysql: mysql}
	return &model
}

func (m *ListModel) execQuery(query string) ([]models.Row, error) {
	db := m.m_mysql.GetConnection()
	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	got := []models.Row{}
	for rows.Next() {
		var r models.Row
		err := rows.Scan(&r.Id, &r.Name, &r.Price)
		if err != nil {
			return nil, err
		}
		got = append(got, r)
	}
	return got, nil
}

func (m *ListModel) GetRandomData() ([]models.Row, error) {
	idList := make([]string, 50)
	for i := range idList {
		idList[i] = strconv.Itoa(randInt(1, 10000))
	}
	sql := "SELECT * FROM bench_data WHERE id in (" + strings.Join(idList, ",") + ")"
	return m.execQuery(sql)
}

func (m *ListModel) GetFirstPageData() ([]models.Row, error) {
	sql := "SELECT * FROM bench_data WHERE id <= 50"
	return m.execQuery(sql)
}
