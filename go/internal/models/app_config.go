package models

type AppConfig struct {
	User         string `env:"DB_USERNAME,required"`
	Pass         string `env:"DB_PASSWORD,required"`
	Host         string `env:"DB_HOSTNAME,required"`
	DbName       string `env:"DB_NAME,required"`
	FastHTTPPort string `env:"FAST_HTTP_PORT"  envDefault:"1236"`
	VanilaPort   string `env:"VANILA_PORT"  envDefault:"1236"`
}
