package vanilahandlers

import (
	"fmt"
	"net/http"
)

func (ac *AppContext) helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello!")
}
