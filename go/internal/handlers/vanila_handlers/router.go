package vanilahandlers

import (
	"github.com/gorilla/mux"
)

func CreateRouter(context AppContext) (*mux.Router, error) {
	mwr := mux.NewRouter()
	internal := mwr.PathPrefix("/api/v1").Methods("GET").Subrouter()
	internal.HandleFunc("/hello", context.helloHandler)
	internal.HandleFunc("/random_list", context.randomListHandler)
	internal.HandleFunc("/first_page_list", context.firstPageListHandler)
	return mwr, nil
}
