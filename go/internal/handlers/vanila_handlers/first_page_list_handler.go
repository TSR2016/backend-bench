package vanilahandlers

import (
	"backend-bench/go/internal/controllers"
	"backend-bench/go/internal/repositories"
	"backend-bench/go/internal/services"
	"backend-bench/go/internal/views"
	"log"
	"net/http"
)

func (ac *AppContext) firstPageListHandler(w http.ResponseWriter, r *http.Request) {
	mysqlAdapter := services.CreateMysqlAdapter(ac.Db)
	repository := repositories.CreateListModel(mysqlAdapter)
	view := views.CreateListView(w)
	controller := controllers.CreateListController(repository, view)

	err := controller.ShowFirstPageData()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Print(err)
	}
}
