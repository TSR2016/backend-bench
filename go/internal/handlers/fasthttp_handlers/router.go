package fasthttphandlers

import "github.com/valyala/fasthttp"

func (ac *AppContext) MainHandler(ctx *fasthttp.RequestCtx) {
	switch string(ctx.Path()) {
	case "/hello":
		ac.helloHandler(ctx)
	case "/random_list":
		ac.randomListHandler(ctx)
	case "/first_page_list":
		ac.firstPageListHandler(ctx)
	default:
		ctx.Error("Unsupported path", fasthttp.StatusNotFound)
	}
}
