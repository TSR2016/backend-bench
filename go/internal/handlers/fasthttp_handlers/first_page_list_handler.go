package fasthttphandlers

import (
	"backend-bench/go/internal/controllers"
	"backend-bench/go/internal/repositories"
	"backend-bench/go/internal/services"
	"backend-bench/go/internal/views"
	"log"

	"github.com/valyala/fasthttp"
)

func (ac *AppContext) firstPageListHandler(ctx *fasthttp.RequestCtx) {
	mysqlAdapter := services.CreateMysqlAdapter(ac.Db)
	repository := repositories.CreateListModel(mysqlAdapter)
	view := views.CreateFastListView(ctx)
	controller := controllers.CreateListController(repository, view)

	err := controller.ShowFirstPageData()
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		log.Print(err)
	}
}
