package fasthttphandlers

import (
	"fmt"

	"github.com/valyala/fasthttp"
)

func (ac *AppContext) helloHandler(ctx *fasthttp.RequestCtx) {
	fmt.Fprintf(ctx, "Hello!")
}
