package services

import (
	"database/sql"

	"backend-bench/go/internal/models"

	_ "github.com/go-sql-driver/mysql"
)

type MysqlAdapter struct {
	m_db *sql.DB
}

func GetSqlDb(params *models.AppConfig) *sql.DB {
	dsn := "" + params.User + ":" + params.Pass + "@tcp(" + params.Host + ":3306)/" + params.DbName
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}
	db.SetMaxOpenConns(100)
	return db
}

func CreateMysqlAdapter(db *sql.DB) *MysqlAdapter {
	adapter := MysqlAdapter{m_db: db}
	return &adapter
}

func (a *MysqlAdapter) GetConnection() *sql.DB {
	return a.m_db
}
