package services

import (
	"fmt"
	"os"

	"backend-bench/go/internal/models"

	_ "github.com/go-sql-driver/mysql"

	"github.com/caarlos0/env/v6"
	"github.com/joho/godotenv"
)

func ParseEnv() *models.AppConfig {
	config := &models.AppConfig{}
	pwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	err = godotenv.Load(pwd + "/.env")
	if err != nil {
		panic(fmt.Errorf("failed load env file: %w", err))
	}
	if err = env.Parse(config); err != nil {
		panic(fmt.Errorf("failed parse env file: %w", err))
	}
	return config
}
