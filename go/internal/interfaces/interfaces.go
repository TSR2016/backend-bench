package interfaces

import (
	"database/sql"

	"backend-bench/go/internal/models"
)

type ListModel interface {
	GetRandomData() ([]models.Row, error)
	GetFirstPageData() ([]models.Row, error)
}

type ListView interface {
	RenderJson(data []models.Row)
}

type MysqlAdapter interface {
	GetConnection() *sql.DB
}
