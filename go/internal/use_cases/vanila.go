package usecases

import (
	vanilahandlers "backend-bench/go/internal/handlers/vanila_handlers"
	"backend-bench/go/internal/services"
	"fmt"
	"net/http"
	"time"

	graceful "gopkg.in/tylerb/graceful.v1"
)

func StartVanila() {
	config := services.ParseEnv()
	db := services.GetSqlDb(config)

	context := &vanilahandlers.AppContext{Db: db}
	router, err := vanilahandlers.CreateRouter(*context)
	if err != nil {
		panic(err)
	}
	srv := &graceful.Server{
		Timeout: 10 * time.Second,
		Server: &http.Server{
			Addr:    ":" + config.VanilaPort,
			Handler: router,
		},
	}
	fmt.Println(srv.ListenAndServe())
}
