package usecases

import (
	fasthttphandlers "backend-bench/go/internal/handlers/fasthttp_handlers"
	"backend-bench/go/internal/services"

	"github.com/valyala/fasthttp"
)

func StartFastHTTP() {
	config := services.ParseEnv()
	db := services.GetSqlDb(config)
	context := &fasthttphandlers.AppContext{Db: db}
	fasthttp.ListenAndServe(":"+config.FastHTTPPort, context.MainHandler)
}
