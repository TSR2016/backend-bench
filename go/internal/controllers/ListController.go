package controllers

import (
	"backend-bench/go/internal/interfaces"

	_ "github.com/go-sql-driver/mysql"
)

type ListController struct {
	m_model interfaces.ListModel
	m_view  interfaces.ListView
}

func CreateListController(model interfaces.ListModel, view interfaces.ListView) *ListController {
	controller := ListController{m_model: model, m_view: view}
	return &controller
}

func (c *ListController) ShowRandomData() error {
	data, err := c.m_model.GetRandomData()
	if err == nil {
		c.m_view.RenderJson(data)
		return nil
	} else {
		return err
	}
}

func (c *ListController) ShowFirstPageData() error {
	data, err := c.m_model.GetFirstPageData()
	if err == nil {
		c.m_view.RenderJson(data)
		return nil
	} else {
		return err
	}
}
