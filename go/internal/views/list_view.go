package views

import (
	"encoding/json"
	"net/http"

	"backend-bench/go/internal/models"
)

type ListView struct {
	m_writer http.ResponseWriter
}

func CreateListView(writer http.ResponseWriter) *ListView {
	view := ListView{m_writer: writer}
	return &view
}

func (v *ListView) RenderJson(data []models.Row) {
	jsonVal, _ := json.Marshal(data)
	v.m_writer.Header().Set("Content-Type", "application/json")
	v.m_writer.Write(jsonVal)
}
