package views

import (
	"encoding/json"

	"backend-bench/go/internal/models"

	"github.com/valyala/fasthttp"
)

type FastListView struct {
	m_writer *fasthttp.RequestCtx
}

func CreateFastListView(writer *fasthttp.RequestCtx) *FastListView {
	view := FastListView{m_writer: writer}
	return &view
}

func (v *FastListView) RenderJson(data []models.Row) {
	jsonVal, _ := json.Marshal(data)
	v.m_writer.Response.Header.Set("Content-Type", "application/json")
	v.m_writer.SetBody(jsonVal)
}
