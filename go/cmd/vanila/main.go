package main

import (
	usecases "backend-bench/go/internal/use_cases"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	usecases.StartVanila()
}
