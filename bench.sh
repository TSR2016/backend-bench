#!/bin/bash

OPTIND=1

ip_sender=''
ip_reciever=''

while getopts "s:r:" opt; do
	case "$opt" in
	s)	ip_sender=$OPTARG
		;;
	r)	ip_reciever=$OPTARG
		;;
	esac
done

dir=`pwd`	

function screenshot {
	fname=$1 #name
	wmctrl -a System Monitor
	scrot -u "$dir/screen/$fname"
}

function memuse {
	pname=$1
	echo $pname memory usage: `ps -C $pname --no-headers -o rss | xargs | sed -e 's/ /+/g' | bc` Kb
}

export PS1='> '
ulimit -n 100000
mkdir -p screen
gnome-system-monitor &

cpufreq-info
#lstopo &
#sleep 1
#scrot -u "sreen/lstopo.jpg"
#kill $!
iperf -s &
ssh $ip_sender "/usr/local/bin/iperf -c $ip_reciever"
sleep 60
wmctrl -a System Monitor

sudo service php7.0-fpm stop
sudo service nginx stop

#cpp-rest-sdk
echo "cpp-rest-sdk"
cpp/cpp-rest-sdk/build/stable/cpp-rest-sdk ./configDb.json > /dev/null 2>&1 &
pid=$!
sleep 5
memuse cpp-rest-sdk
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1237/hello"
screenshot "cppRestHello.jpg"
memuse cpp-rest-sdk
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1237/first_page_list"
screenshot "cppRestFirst.jpg"
memuse cpp-rest-sdk
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1237/random_list"
screenshot "cppRestRandom.jpg"
memuse cpp-rest-sdk
kill $pid
sleep 5

#crow
echo "crow"
cpp/crow/build/stable/crow ./configDb.json > /dev/null 2>&1  &
pid=$!
sleep 5
memuse crow
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1238/hello"
screenshot "cppCrowHello.jpg"
memuse crow
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1238/first_page_list"
screenshot "cppCrowFirst.jpg"
memuse crow
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1238/random_list"
screenshot "cppCrowRandom.jpg"
memuse crow
kill $pid
sleep 5

#proxygen
echo "proxygen"
cpp/proxygen/build/stable/proxygen ./configDb.json > /dev/null 2>&1  &
pid=$!
sleep 5
memuse proxygen
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:11000/hello"
screenshot "cppProxygenHello.jpg"
memuse proxygen
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:11000/first_page_list"
screenshot "cppProxygenFirst.jpg"
memuse proxygen
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:11000/random_list"
screenshot "cppProxygenRandom.jpg"
memuse proxygen
kill $pid
sleep 5

#elixir/phoenix
echo "elixir/phoenix"
pushd elixir/bench
bash start.sh  &
pid=$!
sleep 10
memuse beam.smp
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:4001/hello"
screenshot "elixirHello.jpg"
memuse beam.smp
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:4001/first_page_data"
screenshot "elixirFirst.jpg"
memuse beam.smp
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:4001/random_data"
screenshot "elixirRandom.jpg"
memuse beam.smp
kill $pid
skill beam.smp
sleep 5
popd

#go
pushd go

#simple go
echo "go"
./main &
pid=$!
sleep 5
memuse main
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1236/hello"
screenshot "goHello.jpg"
memuse main
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1236/first_page_list"
screenshot "goFirst.jpg"
memuse main
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1236/random_list"
screenshot "goRandom.jpg"
memuse main
kill $pid
sleep 5

#go fasthttp
echo "go fasthttp"
./main_fasthttp &
pid=$!
sleep 5
memuse main_fasthttp
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1236/hello"
screenshot "goFastHello.jpg"
memuse main_fasthttp
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1236/first_page_list"
screenshot "goFastFirst.jpg"
memuse main_fasthttp
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1236/random_list"
screenshot "goFastRandom.jpg"
memuse main_fasthttp
kill $pid
sleep 5

popd

#node
echo "node"
pushd node
~/.nvm/nvm.sh install v6.2.2
~/.nvm/nvm.sh use v6.2.2
node index.js > /dev/null 2>&1  &
pid=$!
sleep 5
memuse node
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1235/hello"
screenshot "nodeHello.jpg"
memuse node
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1235/first_page_data"
screenshot "nodeFirst.jpg"
memuse node
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1235/random_data"
screenshot "nodeRandom.jpg"
memuse node
kill $pid
sleep 5
popd

#rust
echo "rust"
pushd rust/target/release
./bench  &
pid=$!
sleep 5
memuse bench
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3000/hello"
screenshot "rustHello.jpg"
memuse bench
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3000/first_page_data"
screenshot "rustFirst.jpg"
memuse bench
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3000/random_data"
screenshot "rustRandom.jpg"
memuse bench
kill $pid
sleep 5
popd

#scala
echo "scala"
pushd spray
sbt run  &
pid=$!
sleep 120
#warmup cache
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3456/hello"
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3456/first_page_list"
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3456/random_list"

memuse java
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3456/hello"
screenshot "scalaHello.jpg"
memuse java
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3456/first_page_list"
screenshot "scalaFirst.jpg"
memuse java
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3456/random_list"
screenshot "scalaRandom.jpg"
memuse java
kill $pid
sleep 5
popd

#urweb
echo "urweb"
pushd urweb
./hello.exe > /dev/null 2>&1  &
pid=$!
sleep 5
memuse hello.exe
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:8080/Hello/main"
screenshot "urHello.jpg"
memuse hello.exe
kill $pid
sleep 5
popd

#vert.x
echo "vert.x"
pushd vert.x
java -jar target/vert-x-bench-1.0-SNAPSHOT-fat.jar > /dev/null 2>&1  &
pid=$!
sleep 5
#warmup cache
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:2345/hello"
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:2345/first_page_list"
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:2345/random_list"
memuse java
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:2345/hello"
screenshot "vertxHello.jpg"
memuse java
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:2345/first_page_list"
screenshot "vertxFirst.jpg"
memuse java
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:2345/random_list"
screenshot "vertxRandom.jpg"
memuse java
kill $pid
sleep 5
popd


#reactphp
echo "reactphp"
pushd php
php react.php &
pid=$!
sleep 5
memuse php
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1337/hello"
screenshot "reactPhpHello.jpg"
memuse php
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1337/first_page_data"
screenshot "reactPhpFirst.jpg"
memuse php
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1337/random_data"
screenshot "reactPhpRandom.jpg"
memuse php
kill $pid
sleep 5
popd

#php
echo "php"
sudo service php7.0-fpm start
sudo service nginx start
sleep 10
memuse nginx
memuse php-fpm7.0
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3256/"
screenshot "phpHello.jpg"
memuse nginx
memuse php-fpm7.0
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3256/first_page_data.php"
screenshot "phpFirst.jpg"
memuse nginx
memuse php-fpm7.0
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:3256/random_data.php"
screenshot "phpRandom.jpg"
memuse nginx
memuse php-fpm7.0
sudo service php7.0-fpm stop
sudo service nginx stop
sleep 5

#vibe.d
echo "vibe.d"
pushd d
./vibed-mysql > /dev/null 2>&1  &
pid=$!
sleep 5
memuse vibed-mysql
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1252/hello"
screenshot "VibedHello.jpg"
memuse vibed-mysql
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1252/first_page_data"
screenshot "VibedFirst.jpg"
memuse vibed-mysql
sleep 5
ssh $ip_sender "/usr/local/bin/wrk -t12 -c400 -d30s http://$ip_reciever:1252/random_data"
screenshot "VibedRandom.jpg"
memuse vibed-mysql
kill $pid
sleep 5
popd