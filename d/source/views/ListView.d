module views.ListView;

import interfaces.IView;
import models.Row;
import std.json;
import vibe.d;

class ListView : IView
{
	HTTPServerResponse *m_res;
	this( HTTPServerResponse *res)
	{
		m_res = res;
	}
	
	void renderJson(Row[] data)
	{
		JSONValue subarr = ["id" : data[0].id];
		subarr.object["name"] = data[0].name;
		subarr.object["price"] = data[0].price;
		JSONValue jj = [ subarr ];
		for (size_t i = 1; i< data.length; i++) {
			JSONValue subarr2 = ["id" : data[i].id];
			subarr2.object["name"] = data[i].name;
			subarr2.object["price"] = data[i].price;
			jj.array ~= subarr2;
		}
		m_res.writeBody(jj.toString());
	}
}