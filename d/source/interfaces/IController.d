module interfaces.IController;

interface IController
{
	void showFirstPageData();
	void showRandomData();
}
