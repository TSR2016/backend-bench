module interfaces.IMysqlAdapter;

import mysql.connection;
import ddbc.all;

interface IMysqlAdapter
{
	ddbc.core.Connection getConnection();
}