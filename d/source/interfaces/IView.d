module interfaces.IView;

import models.Row;

interface IView
{
	void renderJson(Row[] data);
}