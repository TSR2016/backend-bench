module interfaces.IModel;

import models.Row;

interface IModel
{
	Row[50] getFirstPageData();
	Row[50] getRandomData();
}