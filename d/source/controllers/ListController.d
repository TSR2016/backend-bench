module controllers.ListController;

import interfaces.IController;
import interfaces.IModel;
import interfaces.IView;

class ListController : IController
{
	private IModel *m_model;
	private IView *m_view;
	
	this(IModel *model, IView *view) {
		m_model = model;
		m_view = view;
	}
	
	void showFirstPageData()
	{
		auto data = m_model.getFirstPageData();
		m_view.renderJson(data);
	}
	
	void showRandomData()
	{
		auto data = m_model.getRandomData();
		m_view.renderJson(data);
	}
}