module models.ListModel;

import interfaces.IModel;
import interfaces.IMysqlAdapter;
import models.Row;
import ddbc.all;
import std.random;
import std.conv;

class ListModel : IModel
{
	private IMysqlAdapter *m_mysql;
	
	this(IMysqlAdapter *mysql)
	{
		m_mysql = mysql;
	}
	
	Row[50] getFirstPageData()
	{
		Row[50] res;
		size_t i = 0;
		
		auto conn = m_mysql.getConnection();
		scope(exit) conn.close();
		Statement stmt = conn.createStatement;
		scope(exit) stmt.close();
		
		auto rs = stmt.executeQuery("SELECT * FROM bench_data WHERE id <= 50");
		while (rs.next()) {
			Row row = {id: rs.getInt(1), name: rs.getString(2), price: rs.getDouble(3)};
			res[i] = row;
			i++;
		}
		return res;
	}
	
	Row[50] getRandomData()
	{
		Row[50] res;
		size_t i = 0;
		
		auto conn = m_mysql.getConnection();
		scope(exit) conn.close();
		Statement stmt = conn.createStatement;
		scope(exit) stmt.close();
		/*
		idList := make([]string, 50)
    for i := range idList {
        idList[i] = strconv.Itoa(randInt(1, 10000))
    }
    sql := "SELECT * FROM bench_data WHERE id in (" + strings.Join(idList, ",") + ")"
		*/
		string sql = "SELECT * FROM bench_data WHERE id in (";
		for (i = 0; i < 50; i++) {
			if (i != 0) {
				sql ~= ',';
			}
			sql ~= to!string(uniform(1, 10000));
		}
		sql ~= ")";
		i = 0;
		auto rs = stmt.executeQuery(sql);
		while (rs.next()) {
			Row row = {id: rs.getInt(1), name: rs.getString(2), price: rs.getDouble(3)};
			res[i] = row;
			i++;
		}
		return res;
	}
}