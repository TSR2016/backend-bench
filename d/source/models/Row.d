module models.Row;

struct Row
{
	int id;
	string name;
	float price;
}