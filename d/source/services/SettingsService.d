module services.SettingsService;

import std.file;
import std.stdio;
import std.json;
import std.conv;
import models.ConnectionParams;

class SettingsService
{
	ConnectionParams parseSettings(string path)
	{
		string jsonStr = to!string(std.file.read(path));
		JSONValue json = parseJSON(jsonStr);
		ConnectionParams params = {
			m_host: json["host"].str(),
	 		m_name: json["dbName"].str(),
			m_user: json["user"].str(),
			m_pass: json["pass"].str()
		};
		return params;
	}
}