module services.MysqlAdapter;

import interfaces.IMysqlAdapter;
import mysql.connection;
import vibe.core.connectionpool;
import ddbc.all;

class MysqlAdapter : IMysqlAdapter
{
	DataSource *m_ds;
	this(DataSource *ds)
	{
		m_ds = ds;
	}
	
	ddbc.core.Connection getConnection()
	{
		return m_ds.getConnection();
	}
}