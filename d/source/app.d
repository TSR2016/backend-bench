import vibe.d;

import services.SettingsService;
import std.stdio;
import ddbc.all;
import mysql.connection;
import models.ConnectionParams;
import services.MysqlAdapter;
import models.ListModel;
import views.ListView;
import controllers.ListController;
import interfaces.IMysqlAdapter;
import interfaces.IModel;
import interfaces.IView;

DataSource ds;
ConnectionParams conParams;

shared static this()
{
	runTask({
		initDb();
	});
	auto settings = new HTTPServerSettings;
	settings.port = 1252;
	settings.bindAddresses = ["::1", "0.0.0.0"];
	listenHTTP(settings, &router);

	logInfo("Please open http://127.0.0.1:1252/ in your browser.");
}

void initDb()
{
	auto ss = new SettingsService();
	conParams = ss.parseSettings("../configDb.json");
	MySQLDriver driver = new MySQLDriver();
	string url = MySQLDriver.generateUrl(conParams.m_host, 3306, conParams.m_name);
	string[string] paramsArr = MySQLDriver.setUserAndPassword(conParams.m_user, conParams.m_pass);
	
	ds = new ConnectionPoolDataSourceImpl(driver, url, paramsArr);
}

void router(HTTPServerRequest req, HTTPServerResponse res)
{
	if (req.path == "/hello") {
		res.writeBody("Hello, World!");
	} else if (req.path == "/first_page_data") {
		IMysqlAdapter mysql = cast(IMysqlAdapter) new MysqlAdapter(&ds);
		IModel model = cast(IModel) new ListModel(&mysql);
		IView view = cast(IView) new ListView(&res);
		auto controller = new ListController(&model, &view);
		
		controller.showFirstPageData();
	} else if (req.path == "/random_data") {
		IMysqlAdapter mysql = cast(IMysqlAdapter) new MysqlAdapter(&ds);
		IModel model = cast(IModel) new ListModel(&mysql);
		IView view = cast(IView) new ListView(&res);
		auto controller = new ListController(&model, &view);
		
		controller.showRandomData();
	} 
}
