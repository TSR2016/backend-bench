import json
import os
import pymysql.cursors
import random
import string

def file_get_contents(filename):
    with open(filename) as f:
        return f.read()

def get_random_string(N):
	return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))

def main():
	jsonContent = file_get_contents(os.getcwd() + "/configDb.json")
	jsonArr = json.loads(jsonContent)
	connection = pymysql.connect(host=jsonArr['host'], 
								 user=jsonArr['user'],
								 password=jsonArr['pass'])
	cursor = connection.cursor()
	cursor.execute('drop database if exists ' + jsonArr['dbName'])
	cursor.execute('create database ' + jsonArr['dbName'])
	cursor.execute('use ' + jsonArr['dbName'])
	cursor.execute( 'create table bench_data('
				    'id int primary key auto_increment,'
				    'name varchar(255),'
				    'price numeric(15,2)'
				    ')')
	for _ in range(10000):
		query = 'insert into bench_data(`name`,`price`) values("' + get_random_string(16) + '", ' + ("%.2f" % random.uniform(10000.00, 50000.00)) +')'
		cursor.execute( query)
		
	connection.commit()

main()