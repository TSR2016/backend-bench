defmodule Bench.ListView do
  use Bench.Web, :view
  
  def render("firstPage.json", %{benchData: benchData}) do
    benchData
  end
end