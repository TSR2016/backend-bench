defmodule Bench.ListController do
  use Bench.Web, :controller
  alias Bench.Repo
  alias Bench.BenchData
  
  def firstPage(conn, _params) do
    query = from b in BenchData, where: b.id <= 50
    benchData = Repo.all(query)
    json conn, %{benchData: benchData }
  end
  
  def random(conn, _params) do
    ids = for _ <- 1..50, do: :rand.uniform(10000)
    query = from b in BenchData, where: b.id in ^ids
    benchData = Repo.all(query)
    json conn, %{benchData: benchData }
  end
end
