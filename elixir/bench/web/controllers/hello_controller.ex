defmodule Bench.HelloController do
  use Bench.Web, :controller

  def index(conn, _params) do
    text conn, "hello"
  end
end
