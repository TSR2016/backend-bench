defmodule Bench.Router do
  use Bench.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Bench do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/hello", HelloController, :index
    get "/first_page_data", ListController, :firstPage
    get "/random_data", ListController, :random
  end

  # Other scopes may use custom stacks.
  # scope "/api", Bench do
  #   pipe_through :api
  # end
end
