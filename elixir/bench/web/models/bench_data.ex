defmodule Bench.BenchData do
  use Ecto.Schema
  
  @derive {Poison.Encoder, except: [:__meta__]}
  schema "bench_data" do
    field :name, :string
	  field :price, :float
	
  end
end