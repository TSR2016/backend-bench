extern crate rustc_serialize;

use interfaces::IView;
use models::row::Row;
use self::rustc_serialize::json;

pub struct ListView {
}

impl IView for ListView {
	fn render_json(&self, data: Vec<Row>) -> String {
		return json::encode(&data).unwrap();
	}
}