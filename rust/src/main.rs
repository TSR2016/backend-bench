
extern crate rustc_serialize;
extern crate iron;

extern crate router;
extern crate r2d2;
extern crate r2d2_mysql;
extern crate persistent;

use iron::prelude::*;
use iron::status;

use router::Router;
use r2d2::Pool;
use r2d2_mysql::MysqlConnectionManager;
use persistent::Read;
use iron::typemap::Key;

mod interfaces;
mod models;
mod services;
mod views;
mod controllers;

use services::mysql_adapter::MysqlAdapter;
use services::settings_service::SettingsService;
use models::list_model::ListModel;
use views::list_view::ListView;
use controllers::list_controller::ListController;
use interfaces::IMysqlAdapter;
use interfaces::IModel;
use interfaces::IView;

pub struct AppDb;
impl Key for AppDb {
    type Value = Pool<MysqlConnectionManager>;
}

fn main() {
    let mut router = Router::new();
    router.get("/hello", hello_handler);
    router.get("/first_page_data", first_page_handler);
    router.get("/random_data", random_page_handler);
    
    let mut chain = Chain::new(router);
    
    let mut db_url = String::from("mysql://");
    let ss = SettingsService{};
    let params = ss.parse_settings("../../../configDb.json");
    db_url.push_str(&params.m_user);
    db_url.push_str(":");
    db_url.push_str(&params.m_pass);
    db_url.push_str("@");
    db_url.push_str(&params.m_host);
    db_url.push_str(":3306/");
    db_url.push_str(&params.m_name);
    
    let config = r2d2::Config::builder()
        .pool_size(100)
        .build();
    
    let manager = r2d2_mysql::MysqlConnectionManager::new(&db_url).unwrap();
    
    let pool = r2d2::Pool::new(config, manager).unwrap();
    
    chain.link(Read::<AppDb>::both(pool));
    
    Iron::new(chain).http("0.0.0.0:3000").unwrap();
}

fn hello_handler(_: &mut Request) -> IronResult<Response> {
    Ok(Response::with((iron::status::Ok, "Hello world!")))
}

fn first_page_handler(req: &mut Request) -> IronResult<Response> {
    let ref pool = req.get::<Read<AppDb>>().unwrap();
    let mysql = Box::new(MysqlAdapter::new(pool));
    let model = Box::new(ListModel::new(mysql as Box<IMysqlAdapter>));
    let view = Box::new(ListView{});
    let controller = ListController::new(model as Box<IModel>, view as Box<IView>);
    Ok(Response::with((status::Ok, controller.show_first_page_data())))
}

fn random_page_handler(req: &mut Request) -> IronResult<Response> {
    let ref pool = req.get::<Read<AppDb>>().unwrap();
    let mysql = Box::new(MysqlAdapter::new(pool));
    let model = Box::new(ListModel::new(mysql as Box<IMysqlAdapter>));
    let view = Box::new(ListView{});
    let controller = ListController::new(model as Box<IModel>, view as Box<IView>);
    Ok(Response::with((status::Ok, controller.show_random_data())))
}