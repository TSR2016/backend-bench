extern crate mysql;
extern crate rand;

use interfaces::IModel;
use interfaces::IMysqlAdapter;
use models::row::Row;
use self::rand::Rng;

pub struct ListModel {
	m_mysql: Box<IMysqlAdapter>,
}

impl ListModel {
	pub fn new(mysql: Box<IMysqlAdapter>) -> ListModel {
		ListModel {
			m_mysql: mysql,
		}
	}
}

impl IModel for ListModel {
	fn get_first_page_data(&self) -> Vec<Row> {
		let mut conn = self.m_mysql.get_connection();
		let rows: Vec<Row> = 
			conn.query("SELECT * FROM bench_data WHERE id <= 50")
			.map(|result| {
				result.map(|x| x.unwrap()).map(|mut row| {
					Row {
						id: row.get(0).unwrap(),
						name: row.get(1).unwrap(),
						price: row.get(2).unwrap(),
					}
				}).collect()
			}).unwrap();
		rows
	}
	
	fn get_random_data(&self) -> Vec<Row> {
		let mut conn = self.m_mysql.get_connection();
		let mut sql = String::from("SELECT * FROM bench_data WHERE id in (");
		for i in 0..50 {
			if i != 0 { 
				sql.push_str(",");
			}
			let num = rand::thread_rng().gen_range(1, 10000);
			sql.push_str(&num.to_string().to_owned());
		}
		sql.push_str(")");
		let rows: Vec<Row> = 
			conn.query(sql)
			.map(|result| {
				result.map(|x| x.unwrap()).map(|mut row| {
					Row {
						id: row.get(0).unwrap(),
						name: row.get(1).unwrap(),
						price: row.get(2).unwrap(),
					}
				}).collect()
			}).unwrap();
		rows
	}
}