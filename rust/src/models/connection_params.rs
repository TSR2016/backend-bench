pub struct ConnectionParams {
	pub m_host: String,
	pub m_name: String,
	pub m_user: String,
	pub m_pass: String,
}