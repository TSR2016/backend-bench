#[derive(RustcEncodable)]
pub struct Row {
	pub id: i32,
	pub name: String,
	pub price: f32
}