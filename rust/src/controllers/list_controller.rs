use interfaces::IModel;
use interfaces::IView;

pub struct ListController {
	m_model: Box<IModel>,
	m_view: Box<IView>,
}


impl ListController {
	pub fn new(model: Box<IModel>, view: Box<IView>) -> ListController {
		ListController {
			m_model: model,
			m_view: view,
		}
	}
}

impl ListController {
	pub fn show_first_page_data(&self) -> String {
		let vec = self.m_model.get_first_page_data();
		self.m_view.render_json(vec)
	}
	
	pub fn show_random_data(&self) -> String {
		let vec = self.m_model.get_random_data();
		self.m_view.render_json(vec)
	}
}