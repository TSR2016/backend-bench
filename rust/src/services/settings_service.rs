extern crate rustc_serialize;
use self::rustc_serialize::json;

use std::fs::File;
use std::io::Read;

use models::connection_params::ConnectionParams;

pub struct SettingsService {
}

impl SettingsService {
	pub fn parse_settings(&self, path: &str) -> ConnectionParams {
		let mut file = File::open(path).unwrap();
		let mut data = String::new();
		file.read_to_string(&mut data).unwrap();
		let json = json::Json::from_str(&data).unwrap();//json::decode(&data).unwrap();
		let params = ConnectionParams { 
			m_host : String::from(json.find_path(&["host"]).unwrap().as_string().unwrap()),
			m_name : String::from(json.find_path(&["dbName"]).unwrap().as_string().unwrap()),
			m_user : String::from(json.find_path(&["user"]).unwrap().as_string().unwrap()),
			m_pass : String::from(json.find_path(&["pass"]).unwrap().as_string().unwrap())
		};
		return params;
	}
}