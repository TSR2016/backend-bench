extern crate r2d2;
extern crate r2d2_mysql;
extern crate mysql;

use interfaces::IMysqlAdapter;
use std::sync::Arc;

pub struct MysqlAdapter {
	m_pool: Arc<r2d2::Pool<r2d2_mysql::MysqlConnectionManager>>,
}

impl MysqlAdapter {
	pub fn new(pool: &Arc<r2d2::Pool<r2d2_mysql::MysqlConnectionManager>>) -> MysqlAdapter {
		MysqlAdapter {
			m_pool: pool.clone(),
		}
	}
}

impl IMysqlAdapter for MysqlAdapter {
	fn get_connection(&self) -> r2d2::PooledConnection<r2d2_mysql::MysqlConnectionManager> {
		return self.m_pool.get().unwrap();
	}
}