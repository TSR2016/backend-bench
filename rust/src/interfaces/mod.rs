use std::vec::Vec;
use models::row::Row;

extern crate r2d2;
extern crate r2d2_mysql;

pub trait IModel {
	fn get_first_page_data(&self) -> Vec<Row>;
	fn get_random_data(&self) -> Vec<Row>;
}

pub trait IMysqlAdapter {
	fn get_connection(&self) -> r2d2::PooledConnection<r2d2_mysql::MysqlConnectionManager>;
}

pub trait IView {
	fn render_json(&self, Vec<Row>) -> String;
}