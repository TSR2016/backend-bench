<?php

require 'vendor/autoload.php';
define("ROOT", dirname(__FILE__));

use Controllers\ListController;
use Models\ListModel;
use Services\MysqlAdapter;
use Views\ListView;

$app = function ($request, $response) {
	$path = $request->getPath();
	switch($path) {
		case "/hello":
			$response->writeHead(200, array('Content-Type' => 'text/plain'));
			$response->end("Hello");
			break;
		case "/first_page_data":
			try {
				$configFile = ROOT . "/../configDb.json";
				$configData = json_decode(file_get_contents($configFile), true);
				$mysql = new MysqlAdapter($configData['user'], $configData['pass'], $configData['dbName'], $configData['host']);
				$model = new ListModel($mysql);
				$view = new ListView();
				$controller = new ListController($model, $view);
				$result = $controller->showFirstPageData();
				$response->writeHead(200, array('Content-Type' => 'application/json'));
				$response->end($result);
				break;
			} catch (Exception $e) {
				$response->writeHead(500, array('Content-Type' => 'text/plain'));
				$response->end("Error: " . $e->getMessage());
			}
			break;
		case "/random_data":
			try {
				$configFile = ROOT . "/../configDb.json";
				$configData = json_decode(file_get_contents($configFile), true);
				$mysql = new MysqlAdapter($configData['user'], $configData['pass'], $configData['dbName'], $configData['host']);
				$model = new ListModel($mysql);
				$view = new ListView();
				$controller = new ListController($model, $view);
				$result = $controller->showRandomData();
				$response->writeHead(200, array('Content-Type' => 'application/json'));
				$response->end($result);
				break;
			} catch (Exception $e) {
				$response->writeHead(500, array('Content-Type' => 'text/plain'));
				$response->end("Error: " . $e->getMessage());
			}
			break;
		default:
			$response->writeHead(404, array('Content-Type' => 'text/plain'));
			$response->end("Not found");
	}
};

$loop = React\EventLoop\Factory::create();
$socket = new React\Socket\Server($loop);
$http = new React\Http\Server($socket, $loop);

$http->on('request', $app);
echo "Server running at http://127.0.0.1:1337\n";

$socket->listen(1337, '0.0.0.0');
$loop->run();