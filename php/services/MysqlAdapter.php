<?php
namespace Services;

use Interfaces\IMysqlAdapter;

class MysqlAdapter implements IMysqlAdapter
{
	private $connection = null;
	
	public function __construct($user, $password, $dbName, $host)
	{
		$this->connection = new \PDO('mysql:host=' . $host . ';dbname=' . $dbName . ';charset=utf8', $user, $password);
	}
	
	public function query($query) {
		$result = [];
		foreach($this->connection->query($query, \PDO::FETCH_ASSOC) as $row) {
			$result[] = $row;
		}
		return $result;
	}	
}