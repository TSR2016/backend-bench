<?php
define("ROOT", dirname(__FILE__));

require_once (ROOT . "/vendor/autoload.php");

use Controllers\ListController;
use Models\ListModel;
use Services\MysqlAdapter;
use Views\ListView;


try {
	$configFile = ROOT . "/../configDb.json";
	$configData = json_decode(file_get_contents($configFile), true);
	$mysql = new MysqlAdapter($configData['user'], $configData['pass'], $configData['dbName'], $configData['host']);
	$model = new ListModel($mysql);
	$view = new ListView();
	$controller = new ListController($model, $view);
	$data = $controller->showFirstPageData();
	header('Content-Type: application/json');
	echo $data;
} catch (Exception $e) {
	var_dump($e->getMessage());
	die("Error: " . $e->getMessage());
}