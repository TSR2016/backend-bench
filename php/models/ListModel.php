<?php
namespace Models;

use Services\MysqlAdapter;
use Interfaces\IModel;
use Interfaces\IMysqlAdapter;

class ListModel implements IModel 
{
	private $m_mysql = null;
	
	public function __construct(IMysqlAdapter $mysql)
	{
		$this->m_mysql = $mysql;
	}
	
	public function getRandomData() 
	{
		$idList = array_fill(0, 50, 0); // create array with 50 elements
		foreach ($idList as &$val) {
			$val = mt_rand(1, 10000);
		}
		return $this->m_mysql->query("select * from bench_data where id in (" . join(",", $idList) . ")");
	}
    
    public function getFirstPageData()
    {
        return $this->m_mysql->query("select * from bench_data where id <= 50");
    }
}