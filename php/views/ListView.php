<?php

namespace Views;

use Interfaces\IView;

class ListView implements IView
{
	public function renderJson($data)
	{
		return json_encode($data);
	}
}