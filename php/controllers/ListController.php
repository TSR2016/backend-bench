<?php

namespace Controllers;

use Models\ListModel;
use Views\ListView;
use Interfaces\IModel;
use Interfaces\IView;

class ListController 
{
	private $m_model;
	
	private $m_view;
	
	public function __construct(IModel $model, IView $view) 
	{
		$this->m_model = $model;
		$this->m_view = $view;
	}
	
	public function showRandomData() 
	{
		$data = $this->m_model->getRandomData();
		return $this->m_view->renderJson($data);
	}
    
    public function showFirstPageData() 
	{
		$data = $this->m_model->getFirstPageData();
		return $this->m_view->renderJson($data);
	}
}