<?php
namespace Interfaces;

interface IView
{
	public function renderJson($data);
}