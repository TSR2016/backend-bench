<?php
namespace Interfaces;

interface IModel
{
	public function getRandomData();
	public function getFirstPageData();
}