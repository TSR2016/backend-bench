package com.spray.bench.views

import slick.backend._
import com.spray.bench.models._
import play.api.libs.json._
import com.spray.bench.models.Formatters._
import com.spray.bench.traits.ViewInterface

class ListView extends ViewInterface{
    
    def renderJson(data: Seq[Bench]): String = {                
        Json.stringify(Json.toJson(data))
    }
}