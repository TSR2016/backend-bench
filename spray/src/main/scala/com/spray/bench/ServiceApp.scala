package com.spray.bench

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import com.spray.bench.config.Configuration
import com.spray.bench.controllers.BenchRestServiceActor
import spray.can.Http
import com.spray.bench.models.BenchDAO
import com.spray.bench.views.ListView

object ServiceApp extends App with Configuration {

  // create an actor system for application
  implicit val system = ActorSystem("bench-service")

  // create and start rest service actor
  val restService = system.actorOf(Props(new BenchRestServiceActor(new BenchDAO, new ListView)), "rest-endpoint")

  // start HTTP server with rest service actor as a handler
  IO(Http) ! Http.Bind(restService, serviceHost, servicePort)
}
