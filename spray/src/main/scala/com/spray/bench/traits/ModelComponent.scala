package com.spray.bench.traits

import scala.concurrent._
import com.spray.bench.models.Bench
import com.spray.bench.models.Failure

trait ModelInterface {
  def getFirst(): Either[Failure, Future[Seq[Bench]]]
  def getRandom(): Either[Failure, Future[Seq[Bench]]]
}