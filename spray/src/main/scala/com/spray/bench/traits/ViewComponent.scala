package com.spray.bench.traits

import com.spray.bench.models.Bench

trait ViewInterface {
  def renderJson(data: Seq[Bench]): String
}