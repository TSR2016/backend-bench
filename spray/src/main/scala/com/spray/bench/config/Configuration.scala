package com.spray.bench.config

import com.typesafe.config.ConfigFactory
import scala.util.Try
import scala.io.Source
import net.liftweb.json._
import java.io._

case class ConfigParams(user: String, pass: String, host: String, dbName: String)

/**
 * Holds service configuration settings.
 */
trait Configuration {

  implicit val formats = DefaultFormats
  
  /**
   * Application config object.
   */
  val config = ConfigFactory.load()

  /** Host name/address to start service on. */
  lazy val serviceHost = Try(config.getString("service.host")).getOrElse("localhost")

  /** Port to start service on. */
  lazy val servicePort = Try(config.getInt("service.port")).getOrElse(8080)

  lazy val path = new File(".").getAbsolutePath() + "/../configDb.json"
  lazy val source: String = Source.fromFile(path).getLines.mkString
  lazy val json = parse(source)
  lazy val configParams = json.extract[ConfigParams]
  
  /** Database host name/address. */
  lazy val dbHost = configParams.host

  /** Database host port number. */
  lazy val dbPort = 3306

  /** Service database name. */
  lazy val dbName = configParams.dbName

  /** User name used to access database. */
  lazy val dbUser = configParams.user

  /** Password for specified user and database. */
  lazy val dbPassword = configParams.pass
}
