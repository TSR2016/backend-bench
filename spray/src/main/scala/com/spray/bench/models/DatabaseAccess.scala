package com.spray.bench.models

import com.spray.bench.config.Configuration

import slick.driver.MySQLDriver
import slick.driver.MySQLDriver.api._

import com.mchange.v2.c3p0.ComboPooledDataSource

trait DatabaseAccess extends Configuration {
  val Url = s"jdbc:mysql://$dbHost:$dbPort/$dbName"
  val Driver = "com.mysql.jdbc.Driver"

  protected val db = Database.forURL(Url, driver = Driver)

  val dbPool = {
    val ds = new ComboPooledDataSource
    ds.setUser(dbUser); 
    ds.setPassword(dbPassword)
    ds.setDriverClass(Driver)
    ds.setJdbcUrl(Url)
    ds.setMinPoolSize(100)
    ds.setAcquireIncrement(1)
    ds.setMaxPoolSize(100)
    Database.forDataSource(ds)
  }
}