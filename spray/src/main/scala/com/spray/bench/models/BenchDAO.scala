package com.spray.bench.models

import java.sql._

import com.spray.bench.config.Configuration

import slick.driver.MySQLDriver
import slick.driver.MySQLDriver.api._

import slick.jdbc.meta.MTable

import util.Random.nextInt
import scala.concurrent._

import slick.backend._
import scala.concurrent.ExecutionContext.Implicits.global

import com.spray.bench.traits.ModelInterface
/**
 * Provides a DAO for Bench entities.
 */
class BenchDAO extends ModelInterface with DatabaseAccess {
  val tableBenchs = TableQuery[Benchs] 

  def tableName: String = "bench_data"
  def tableSchema: MySQLDriver.DDL = tableBenchs.schema

  /**
   * Retrieves first 50 records.
   *
   * @return bench entities 
   */
  override def getFirst(): Either[Failure, Future[Seq[Bench]]] = {
    try {
      val q = tableBenchs.filter(b => b.id <= 50.toLong).map(b => b)
      // run the query
      Right(dbPool.run(q.result))
    } catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }
  
  /**
   * produce random int.
   *
   * @param Low lower bound
   * @param High higher bound
   * @return random int within bounds
   */
  protected def getRandomLong(Low: Int, High: Int): Long = {
    val r = scala.util.Random
    val rand = r.nextInt(High-Low) + Low;
    rand.toLong
  }

  /**
   * Retrieves random 50 records.
   *
   * @return bench entities
   */
  override def getRandom(): Either[Failure, Future[Seq[Bench]]] = {
    try {
      val id_list = Seq.fill(50)(getRandomLong(1, 10000))
      val q = tableBenchs.filter(c => c.id inSetBind id_list).map(b => b)
      // run the query
      Right(dbPool.run(q.result))
    } catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  /**
   * Produce a database error.
   *
   * @param e SQL Exception
   * @return database error description
   */
  protected def databaseError(e: SQLException) =
    Failure("%d: %s".format(e.getErrorCode, e.getMessage), FailureType.DatabaseFailure)

}
