package com.spray.bench.models

import slick.driver.MySQLDriver
import slick.driver.MySQLDriver.api._

import play.api.libs.json._
/**
 * Bench entity.
 *
 * @param id      some id
 * @param name    some name
 * @param price   some price
 */
case class Bench(id: Long, name: String, price: Float)

/**
 * Mapped notification table object.
 */
class Benchs(tag: Tag) extends Table[Bench](tag, "bench_data") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

  def name = column[String]("name")

  def price = column[Float]("price")

  def * = (id, name, price) <>((Bench.apply _).tupled, Bench.unapply)
}



object Formatters {
  implicit val benchFormat = Json.format[Bench]
}