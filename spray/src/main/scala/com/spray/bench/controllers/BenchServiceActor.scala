package com.spray.bench.controllers

import akka.actor.Actor
import akka.event.slf4j.SLF4JLogging
//import com.spray.bench.models.BenchDAO
import com.spray.bench.models._
import net.liftweb.json.Serialization._
import net.liftweb.json.DefaultFormats
import spray.http._
import spray.httpx.unmarshalling.Unmarshaller
import spray.routing._
import com.spray.bench.traits.ModelInterface
import com.spray.bench.traits.ViewInterface
import scala.concurrent._

/**
 * Bench REST Service actor.
 */
class BenchRestServiceActor(Model: ModelInterface, View: ViewInterface) extends Actor with BenchRestService {

  implicit def actorRefFactory = context

  def receive = runRoute(rest)

  val benchDAO = Model
  val listView = View
}

/**
 * REST Service
 */
trait BenchRestService extends HttpService with SLF4JLogging {
  this: BenchRestServiceActor =>

  //val benchDAO = Model

  implicit val executionContext = actorRefFactory.dispatcher

  implicit val liftJsonFormats = DefaultFormats

  implicit val benchRejectionHandler = RejectionHandler {
    case rejections => mapHttpResponse {
      response =>
        response.withEntity(HttpEntity(ContentType(MediaTypes.`application/json`),
          write(Map("error" -> response.entity.asString))))
    } {
      RejectionHandler.Default(rejections)
    }
  }

  //val listView = View

  val rest = {
    path("hello") {
      get {
        ctx: RequestContext => {
          ctx.complete("hello")
        }
      }
    } ~
    path("first_page_list") {
      get {
        respondWithMediaType(MediaTypes.`application/json`) {
          ctx: RequestContext =>  {
            val data = benchDAO.getFirst()
            handleRequest(ctx, data)
          }
        }
      }
    } ~
    path("random_list") {
      get {
        respondWithMediaType(MediaTypes.`application/json`) {
          ctx: RequestContext => {
            val data = benchDAO.getRandom()
            handleRequest(ctx, data)
          }
        }
      }
    }
  }

  
  /**
   * Handles an incoming request and create valid response for it.
   *
   * @param ctx         request context
   * @param successCode HTTP Status code for success
   * @param action      action to perform
   */
  protected def handleRequest(ctx: RequestContext, data: Either[Failure, Future[Seq[Bench]]]) {
    data match {
      case Right(future: Future[Seq[Bench]]) =>
        future.map(
          result => {
            ctx.complete(listView.renderJson(result))
          }
        )
      case Left(error: Failure) =>
        ctx.complete(error.getStatusCode, net.liftweb.json.Serialization.write(Map("error" -> error.message)))
      case _ =>
        ctx.complete(StatusCodes.InternalServerError)
    }
  }
}
