organization  := "com.spray.bench"

version       := "0.1"

scalaVersion  := "2.11.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

cancelable in Global := true

libraryDependencies ++= {
  val akkaV = "2.4.12"
  val sprayV = "1.3.4"
  Seq(
    "io.spray"            %%  "spray-can"     % sprayV,
    "io.spray"            %%  "spray-routing" % sprayV,
    "io.spray"            %%  "spray-testkit" % sprayV  % "test",
    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test",
    "org.specs2"          %%  "specs2-core"   % "2.3.11" % "test",
    "com.typesafe.slick"  %%  "slick"          % "3.1.1",
    "mysql"               %   "mysql-connector-java"  % "5.1.40",
    "ch.qos.logback"      %   "logback-classic" % "1.1.7",
    "net.liftweb"         %% "lift-json"        % "2.6+",
    "com.typesafe.akka"   %% "akka-slf4j"       % "2.4.12",
    "com.typesafe.play"   %% "play-json"        % "2.5.10",
    "com.mchange"         % "c3p0"              % "0.9.5.1"
  )
}

Revolver.settings