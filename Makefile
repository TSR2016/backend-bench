.PHONY: preparedb  ## run a full build
preparedb:
	echo MYSQL_USER=`cat configDb.json| jq -r .user` > .env
	echo MYSQL_PASSWORD=`cat configDb.json| jq -r .pass` >> .env
	echo MYSQL_DATABASE=`cat configDb.json| jq -r .dbName` >> .env
	docker-compose up -d
	python3 -m pip install pymysql
	python3 createDb.py