/* Copyright 2013 Active911 Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http: *www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/* ConnectionPool manages a connection pool of some kind.  Worker threads can ask for a connection, and must return it when done.
 * Each connection is guaranteed to be healthy, happy, and free of disease.
 *
 * Connection and ConnectionFactory are virtual classes that should be overridden to their actual type.
 *
 * NOTE: To avoid using templates AND inheritance at the same time in the ConnectionFactory, ConnectionFactory::create must create a derved type 
 * but return the base class. 	
 */


// Define your custom logging function by overriding this #define
#ifndef _DEBUG
	#define _DEBUG(x)
#endif



#include <deque>
#include <set>
#include <boost/thread/mutex.hpp>
#include <boost/lockfree/queue.hpp>
#include <exception>
#include <string>
using namespace std;

namespace active911 {


	struct ConnectionUnavailable : std::exception { 

		char const* what() const throw() {

			return "Unable to allocate connection";
		}; 
	};


	class Connection {

	public:
		Connection(){};
		virtual ~Connection(){};
		unsigned pos;
	};

	class ConnectionFactory {

	public:
		virtual shared_ptr<Connection> create()=0;
	};

	struct ConnectionPoolStats {

		size_t pool_size;
		size_t borrowed_size;

	};

	template<class T>
	class ConnectionPool {

	public:

		ConnectionPoolStats get_stats() {

			// Lock
			boost::mutex::scoped_lock lock(this->io_mutex);

			// Get stats
			ConnectionPoolStats stats;
			stats.pool_size=this->pool.size();
			stats.borrowed_size=this->borrowed_size;			

			return stats;
		};

		ConnectionPool(size_t pool_size, shared_ptr<ConnectionFactory> factory){

			// Setup
			this->pool_size=pool_size;
			this->borrowed_size = 0;
			this->factory=factory;

			// Fill the pool
			//unique_ptr<boost::lockfree::queue<unsigned>> freePos
			freePos = make_unique<boost::lockfree::queue<unsigned>>(0);
			freePos->reserve(pool_size);
			this->pool.reserve(pool_size);
			for (unsigned i = 0; i < pool_size; i++) {
				this->pool.emplace_back(this->factory->create());
				this->freePos->push(i);
			}
		};

		~ConnectionPool() {


		};

		/**
		 * Borrow
		 *
		 * Borrow a connection for temporary use
		 *
		 * When done, either (a) call unborrow() to return it, or (b) (if it's bad) just let it go out of scope.  This will cause it to automatically be replaced.
		 * @retval a shared_ptr to the connection object
		 */
		shared_ptr<T> borrow(){
			unsigned pos;
			if (!this->freePos->pop(pos)) {
				throw ConnectionUnavailable();
			}
			shared_ptr<Connection>conn = pool[pos];
			this->borrowed_size++;
			conn->pos = pos;
			return static_pointer_cast<T>(conn);
		};

		/**
		 * Unborrow a connection
		 *
		 * Only call this if you are returning a working connection.  If the connection was bad, just let it go out of scope (so the connection manager can replace it).
		 * @param the connection
		 */
		void unborrow(shared_ptr<T> conn) {
			this->borrowed_size--;
			this->freePos->push(conn->pos);
		};

	protected:
		shared_ptr<ConnectionFactory> factory;
		size_t pool_size;
		std::atomic<int> borrowed_size;
		unique_ptr<boost::lockfree::queue<unsigned>> freePos;
		std::vector<shared_ptr<Connection> > pool;
	};




}