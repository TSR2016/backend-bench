//#include "stdafx.h"
#include <cpprest/http_listener.h>
#include <cpprest/json.h>

#include "common/views/CppRestSdkView.h"
#include "common/controllers/ListController.h"
#include "common/services/SettingsService.h"
#include "common/models/ListModel.h"
#include "common/services/MysqlAdapter.h"
 
using namespace web;
using namespace web::http;
using namespace web::http::experimental::listener;
 
#include <iostream>
#include <map>
#include <set>
#include <string>
using namespace std;
 
#define TRACE(msg)            wcout << msg
#define TRACE_ACTION(a, k, v) wcout << a << L" (" << k << L", " << v << L")\n"
 
map<utility::string_t, utility::string_t> dictionary;

SettingsService settings;
ConnectionParams params;
shared_ptr<MySQLConnectionFactory> mysql_connection_factory;
shared_ptr<ConnectionPool<MySQLConnection> > mysql_pool;

 
/* handlers implementation */
void handle_get(http_request message){
      auto paths = http::uri::split_path(http::uri::decode(message.relative_uri().path()));
      if (paths.empty()) {
            message.reply(status_codes::NotFound);
            return;
      }
      
      if (paths[0] == U("hello")) {
	      message.reply(status_codes::OK, U("Hello, World"));
      } else if(paths[0] == U("first_page_list")) {
            CppRestSdkView view;
            MysqlAdapter mysql(mysql_pool, params.m_name);
            ListModel model(mysql);
            ListController controller(model, view);
            controller.showFirstPageData();
            message.reply(status_codes::OK, view.getResult());
      } else if(paths[0] == U("random_list")) {
            CppRestSdkView view;
            MysqlAdapter mysql(mysql_pool, params.m_name);
            ListModel model(mysql);
            ListController controller(model, view);
            controller.showRandomPageData();
            message.reply(status_codes::OK, view.getResult());
      } else {
            message.reply(status_codes::NotFound);
      }
}

int main(int argc, char* argv[])
{
      if (argc < 2) {
		cerr << "Path to configDb.json should be passed as first parameter\n";
		return 0;
	}
	params = settings.parseSettings(argv[1]);
	mysql_connection_factory = make_shared<MySQLConnectionFactory>(params.m_host,params.m_user,params.m_pass);
	mysql_pool = make_shared<ConnectionPool<MySQLConnection>>(100, mysql_connection_factory);
      
      utility::string_t address = U("http://0.0.0.0:1237/");
      uri_builder uri(address);
    
      auto addr = uri.to_uri().to_string(); 
      http_listener listener(addr);
      
      listener.support(methods::GET, handle_get);
      
      try
      {
            listener
            .open()
            .then([&listener](){TRACE(L"\nstarting to listen\n");})
            .wait();
      
            while (true);
      }
      catch (exception const & e)
      {
      wcout << e.what() << endl;
      }
 
      return 0;
}