#pragma once
#include "crow/includes/crow.h"
#include "common/interfaces/IView.h"

class CrowView : public IView
{
private:
	crow::json::wvalue result;
public:	
	virtual void renderJson(const vector<Row>) override;
	string getResult();
};