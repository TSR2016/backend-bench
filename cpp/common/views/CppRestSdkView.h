#pragma once

#include "common/interfaces/IView.h"
#include <cpprest/json.h>

using namespace web;

class CppRestSdkView : public IView
{
private:
	json::value result;
public:	
	virtual void renderJson(const vector<Row>) override;
	json::value getResult();
};