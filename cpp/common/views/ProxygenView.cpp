#include "common/views/ProxygenView.h"

void ProxygenView::renderJson(const vector<Row> data)
{
	unsigned i = 0;
	for (Row row : data) {
		result[i][U("id")] = json::value::number(row.m_id);
		result[i][U("name")] = json::value::string(U(row.m_name.c_str()));
		result[i][U("price")] = json::value::number(row.m_price);
		i++;
	}
}

string ProxygenView::getResult()
{
	utility::stringstream_t stream;
	result.serialize(stream);
	return utility::conversions::to_utf8string(stream.str());
}