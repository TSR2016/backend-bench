#pragma once

#include "common/interfaces/IView.h"
#include <cpprest/json.h>

using namespace web;
using namespace std;

class ProxygenView : public IView
{
private:
	json::value result;
public:	
	virtual void renderJson(const vector<Row>) override;
	string getResult();
};