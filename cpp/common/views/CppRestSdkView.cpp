#include "common/views/CppRestSdkView.h"

void CppRestSdkView::renderJson(const vector<Row> data)
{
	unsigned i = 0;
	for (Row row : data) {
		result[i][U("id")] = json::value::number(row.m_id);
		result[i][U("name")] = json::value::string(U(row.m_name.c_str()));
		result[i][U("price")] = json::value::number(row.m_price);
		i++;
	}
}

json::value CppRestSdkView::getResult()
{
	return result;
}