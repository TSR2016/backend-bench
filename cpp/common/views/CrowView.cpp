#include "common/views/CrowView.h"

void CrowView::renderJson(const vector<Row> data)
{
	unsigned i = 0;
	for (Row row : data) {
		result[i]["id"] = row.m_id;
		result[i]["name"] = row.m_name;
		result[i]["price"] = row.m_price;
		i++;
	}
}

std::string CrowView::getResult()
{
	return crow::json::dump(result);
}