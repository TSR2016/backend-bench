#include "common/controllers/ListController.h"

using namespace std;

void ListController::showFirstPageData()
{
	auto data = _model.getFirstPageData();
	_view.renderJson(data);
}

void ListController::showRandomPageData()
{
	auto data = _model.getRandomPageData();
	_view.renderJson(data);
}