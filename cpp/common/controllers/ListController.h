#pragma once

#include "common/interfaces/IController.h"

using namespace std;

class ListController : public IController
{
private:
	IModel &_model;
	IView &_view;
public:
	ListController()=delete; 
	
	ListController(IModel &model, IView &view) : _model(model), _view(view) {};
	void showFirstPageData() override;
	void showRandomPageData() override;
};