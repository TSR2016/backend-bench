#pragma once

#include <vector>
#include "common/models/Row.h"
#include "common/interfaces/IMysqlAdapter.h"

using namespace std;

class IModel
{
public:
	virtual vector<Row> getFirstPageData() = 0;
	virtual vector<Row> getRandomPageData() = 0;
	virtual ~IModel()=default;
};   