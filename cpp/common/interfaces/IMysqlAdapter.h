#pragma once

#include "vendor/connection-pool/MySQLConnection.h"

using namespace std;
using namespace active911;

class IMysqlAdapter
{
public:
	virtual shared_ptr<MySQLConnection> getConnection() = 0;
	virtual void freeConnection() = 0;
	virtual ~IMysqlAdapter()=default;
};   