#pragma once

#include "common/interfaces/IModel.h"
#include "common/interfaces/IView.h"

class IController
{
public:
	virtual void showFirstPageData() = 0;
	virtual void showRandomPageData() = 0;
	virtual ~IController()=default;
};   