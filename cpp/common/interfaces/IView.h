#pragma once

#include <vector>
#include "common/models/Row.h"

using namespace std;

class IView
{
public:
	virtual void renderJson(const vector<Row>) = 0;
	virtual ~IView()=default;
};   