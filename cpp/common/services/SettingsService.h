#pragma once

#include "common/models/ConnectionParams.h"
#include <string>

using namespace std;

class SettingsService
{
public:
	ConnectionParams parseSettings(string);
};