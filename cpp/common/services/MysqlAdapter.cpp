#include "common/services/MysqlAdapter.h"

MysqlAdapter::MysqlAdapter(shared_ptr<ConnectionPool<MySQLConnection> > pool, const string dbName) 
{
	this->_pool = pool;
	for (unsigned i = 0; i< 5; i++) {
		try {
			this->_conn = this->_pool->borrow();
			break;
		} catch (active911::ConnectionUnavailable) {
			
		}
	}
	if (!this->_conn) {
		throw active911::ConnectionUnavailable();
	}
	this->_conn->sql_connection->setSchema(dbName);
}

shared_ptr<MySQLConnection> MysqlAdapter::getConnection()
{
	return this->_conn;
}

void MysqlAdapter::freeConnection()
{
	if (this->_conn) {
		this->_pool->unborrow(this->_conn);
		this->_conn.reset();
	}
}

MysqlAdapter::~MysqlAdapter()
{
	freeConnection();
} 