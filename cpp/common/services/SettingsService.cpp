#include "common/services/SettingsService.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

using namespace std;
using namespace boost::property_tree;

ConnectionParams SettingsService::parseSettings(string path)
{
	ifstream ifs(path);
	if (!ifs) {
		throw runtime_error(string("Failed to open settings json: ") + path);
	}
	ptree pt;
	read_json(ifs, pt);
	ConnectionParams params;
	params.m_host = pt.get<string>("host");
	params.m_name = pt.get<string>("dbName");
	params.m_user = pt.get<string>("user");
	params.m_pass = pt.get<string>("pass");
	return params;
}