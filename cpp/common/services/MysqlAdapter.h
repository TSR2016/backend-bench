#pragma once

#include "common/interfaces/IMysqlAdapter.h"

using namespace std;

class MysqlAdapter : public IMysqlAdapter
{
private:
	shared_ptr<MySQLConnection> _conn;
	shared_ptr<ConnectionPool<MySQLConnection> > _pool;
public:
	MysqlAdapter()=delete; 
	virtual ~MysqlAdapter();
	MysqlAdapter(shared_ptr<ConnectionPool<MySQLConnection> >, const string);
	virtual shared_ptr<MySQLConnection> getConnection() override;
	virtual void freeConnection() override;
};