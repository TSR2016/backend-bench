#pragma once

#include "common/interfaces/IModel.h"

using namespace std;

class ListModel : public IModel
{
private:
	IMysqlAdapter &_mysql;
public:
	ListModel()=delete; 
	
	ListModel(IMysqlAdapter &mysql) : _mysql(mysql) {};
	virtual vector<Row> getFirstPageData();
	virtual vector<Row> getRandomPageData();
};