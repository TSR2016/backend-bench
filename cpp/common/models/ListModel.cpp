#include "common/models/ListModel.h"

using namespace std;

vector<Row> ListModel::getFirstPageData()
{
	auto pconn = this->_mysql.getConnection();
	unique_ptr<sql::PreparedStatement> pstmt(pconn->sql_connection->prepareStatement("select * from bench_data where id <= 50"));
	unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
	vector<Row> resultArray;
	resultArray.reserve(50);
	while (res->next()) {
		resultArray.emplace_back(res->getInt("id"), res->getString("name"), res->getDouble("price"));
	}
	this->_mysql.freeConnection();
	return resultArray;
}

vector<Row> ListModel::getRandomPageData()
{
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> dis(1, 10000);
	string ids("select * from bench_data where id in (");
	ids.reserve(ids.capacity() + 50 * (5 + 1) + 1);
	for (unsigned i = 0; i < 50; i++) {
		ids.append(to_string(dis(gen)));
		ids.push_back(',');
	}
	ids.pop_back();
	ids.push_back(')');
	auto pconn = this->_mysql.getConnection();
	unique_ptr<sql::PreparedStatement> pstmt(pconn->sql_connection->prepareStatement(ids.c_str()));
	unique_ptr<sql::ResultSet> res(pstmt->executeQuery());
	vector<Row> resultArray;
	resultArray.reserve(50);
	while (res->next()) {
		resultArray.emplace_back(res->getInt("id"), res->getString("name"), res->getDouble("price"));
	}
	this->_mysql.freeConnection();
	return resultArray;
}