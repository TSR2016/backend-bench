#pragma once

#include <string>

using namespace std;

struct ConnectionParams
{
	string m_host;
	string m_name;
	string m_user;
	string m_pass;
};