#pragma once

#include <string>

using namespace std;

struct Row
{
	int m_id;
	string m_name;
	float m_price;
	Row(int id, string name, float price) : m_id(id), m_name(name), m_price(price) {};
};