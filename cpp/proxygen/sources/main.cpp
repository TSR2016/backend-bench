/*
 *  Copyright (c) 2016, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 *
 */
#include <gflags/gflags.h>
#include <folly/Memory.h>
#include <folly/Portability.h>
#include <folly/io/async/EventBaseManager.h>
#include <proxygen/httpserver/HTTPServer.h>
#include <proxygen/httpserver/RequestHandlerFactory.h>
#include <unistd.h>

#include "DefaultHandler.h"
#include "HelloHandler.h"
#include "RandomListHandler.h"
#include "FirstPageListHandler.h"

#include "common/services/SettingsService.h"
#include "common/services/MysqlAdapter.h"

using namespace HelloService;
using namespace DefaultService;
using namespace RandomListService;
using namespace FirstPageListService;
using namespace proxygen;

using folly::EventBase;
using folly::EventBaseManager;
using folly::SocketAddress;

using Protocol = HTTPServer::Protocol;

DEFINE_int32(http_port, 11000, "Port to listen on with HTTP protocol");
DEFINE_string(ip, "0.0.0.0", "IP/Hostname to bind to");
DEFINE_int32(threads, 0, "Number of threads to listen on. Numbers <= 0 "
             "will use the number of cores on this machine.");

SettingsService settings;
ConnectionParams params;
shared_ptr<MySQLConnectionFactory> mysql_connection_factory;
shared_ptr<ConnectionPool<MySQLConnection> > mysql_pool;

class ServerHandlerFactory : public RequestHandlerFactory {
public:
  void onServerStart(folly::EventBase* evb) noexcept override {
  }
  
  void onServerStop() noexcept override {
  }
  
  RequestHandler* onRequest(RequestHandler* h, HTTPMessage* msg) noexcept override {
    if (msg->getPath() == "/hello") {
      return new HelloHandler();
    } else if (msg->getPath() == "/first_page_list") {
      return new FirstPageListHandler(mysql_pool, params);
    } else if (msg->getPath() == "/random_list") {
      return new RandomListHandler(mysql_pool, params);
    } else {
      return new DefaultHandler();
    }
  }

private:
  
};

int main(int argc, char* argv[]) {
  if (argc < 2) {
		cerr << "Path to configDb.json should be passed as first parameter\n";
		return 0;
	}
	
  params = settings.parseSettings(argv[1]);
  mysql_connection_factory = make_shared<MySQLConnectionFactory>(params.m_host,params.m_user,params.m_pass);
  mysql_pool = make_shared<ConnectionPool<MySQLConnection>>(100, mysql_connection_factory);
  
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  google::InitGoogleLogging(argv[0]);
  google::InstallFailureSignalHandler();

  std::vector<HTTPServer::IPConfig> IPs = {
    {SocketAddress(FLAGS_ip, FLAGS_http_port, true), Protocol::HTTP},
  };

  if (FLAGS_threads <= 0) {
    FLAGS_threads = sysconf(_SC_NPROCESSORS_ONLN);
    CHECK(FLAGS_threads > 0);
  }

  HTTPServerOptions options;
  options.threads = static_cast<size_t>(FLAGS_threads);
  options.idleTimeout = std::chrono::milliseconds(60000);
  options.shutdownOn = {SIGINT, SIGTERM};
  options.enableContentCompression = true;
  options.handlerFactories = RequestHandlerChain()
      .addThen<ServerHandlerFactory>()
      .build();

  HTTPServer server(std::move(options));
  server.bind(IPs);

  // Start HTTPServer mainloop in a separate thread
  std::thread t([&] () {
    server.start();
  });

  t.join();
  return 0;
}
