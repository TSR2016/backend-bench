/*
 *  Copyright (c) 2016, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 *
 */
#include "DefaultHandler.h"

#include <proxygen/httpserver/RequestHandler.h>
#include <proxygen/httpserver/ResponseBuilder.h>

using namespace proxygen;

namespace DefaultService {

void DefaultHandler::onRequest(std::unique_ptr<HTTPMessage> headers) noexcept {
}

void DefaultHandler::onBody(std::unique_ptr<folly::IOBuf> body) noexcept {
  if (body_) {
    body_->prependChain(std::move(body));
  } else {
    body_ = std::move(body);
  }
}

void DefaultHandler::onEOM() noexcept {
  ResponseBuilder(downstream_)
    .status(404, "Not found")
    .body("Not found")
    .sendWithEOM();
}

void DefaultHandler::onUpgrade(UpgradeProtocol protocol) noexcept {
  // handler doesn't support upgrades
}

void DefaultHandler::requestComplete() noexcept {
  delete this;
}

void DefaultHandler::onError(ProxygenError err) noexcept {
  delete this;
}

}
