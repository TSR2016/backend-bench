/*
 *  Copyright (c) 2016, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 *
 */
#include "FirstPageListHandler.h"
#include "common/views/ProxygenView.h"
#include "common/controllers/ListController.h"
#include "common/services/SettingsService.h"
#include "common/models/ListModel.h"
#include "common/services/MysqlAdapter.h"

#include <proxygen/httpserver/RequestHandler.h>
#include <proxygen/httpserver/ResponseBuilder.h>

using namespace proxygen;

namespace FirstPageListService {
  
  void FirstPageListHandler::onRequest(std::unique_ptr<HTTPMessage> headers) noexcept {
  }
  
  void FirstPageListHandler::onBody(std::unique_ptr<folly::IOBuf> body) noexcept {
    if (body_) {
      body_->prependChain(std::move(body));
    } else {
      body_ = std::move(body);
    }
  }
  
  void FirstPageListHandler::onEOM() noexcept {
    ProxygenView view;
		MysqlAdapter mysql(_mysql_pool, _params.m_name);
		ListModel model(mysql);
		ListController controller(model, view);
		controller.showFirstPageData();
		ResponseBuilder(downstream_)
      .status(200, "OK")
      .body(view.getResult())
      .sendWithEOM();
  }
  
  void FirstPageListHandler::onUpgrade(UpgradeProtocol protocol) noexcept {
    // handler doesn't support upgrades
  }
  
  void FirstPageListHandler::requestComplete() noexcept {
    delete this;
  }
  
  void FirstPageListHandler::onError(ProxygenError err) noexcept {
    delete this;
  }

}
