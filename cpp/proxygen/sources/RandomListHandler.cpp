/*
 *  Copyright (c) 2016, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 *
 */
#include "RandomListHandler.h"
#include "common/views/ProxygenView.h"
#include "common/controllers/ListController.h"
#include "common/services/SettingsService.h"
#include "common/models/ListModel.h"
#include "common/services/MysqlAdapter.h"


#include <proxygen/httpserver/RequestHandler.h>
#include <proxygen/httpserver/ResponseBuilder.h>

using namespace proxygen;

namespace RandomListService {

  void RandomListHandler::onRequest(std::unique_ptr<HTTPMessage> headers) noexcept {
  }
  
  void RandomListHandler::onBody(std::unique_ptr<folly::IOBuf> body) noexcept {
    if (body_) {
      body_->prependChain(std::move(body));
    } else {
      body_ = std::move(body);
    }
  }
  
  void RandomListHandler::onEOM() noexcept {
    ProxygenView view;
		MysqlAdapter mysql(_mysql_pool, _params.m_name);
		ListModel model(mysql);
		ListController controller(model, view);
		controller.showRandomPageData();
		ResponseBuilder(downstream_)
      .status(200, "OK")
      .body(view.getResult())
      .sendWithEOM();
  }
  
  void RandomListHandler::onUpgrade(UpgradeProtocol protocol) noexcept {
    // handler doesn't support upgrades
  }
  
  void RandomListHandler::requestComplete() noexcept {
    delete this;
  }
  
  void RandomListHandler::onError(ProxygenError err) noexcept {
    delete this;
  }

}
