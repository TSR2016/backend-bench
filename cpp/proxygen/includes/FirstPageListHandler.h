/*
 *  Copyright (c) 2016, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 *
 */
#pragma once

#include <folly/Memory.h>
#include <proxygen/httpserver/RequestHandler.h>
#include "common/services/MysqlAdapter.h"
#include "common/models/ConnectionParams.h"

namespace proxygen {
  class ResponseHandler;
}

namespace FirstPageListService {

  class FirstPageListHandler : public proxygen::RequestHandler {
  public:
      FirstPageListHandler(shared_ptr<ConnectionPool<MySQLConnection> > mysql_pool, ConnectionParams &params) : _mysql_pool(mysql_pool), _params(params) {}
      FirstPageListHandler()=delete;
      void onRequest(std::unique_ptr<proxygen::HTTPMessage> headers)
          noexcept override;
      
      void onBody(std::unique_ptr<folly::IOBuf> body) noexcept override;
      
      void onEOM() noexcept override;
      
      void onUpgrade(proxygen::UpgradeProtocol proto) noexcept override;
      
      void requestComplete() noexcept override;
      
      void onError(proxygen::ProxygenError err) noexcept override;
  
  private:
      std::unique_ptr<folly::IOBuf> body_;
      shared_ptr<ConnectionPool<MySQLConnection> > _mysql_pool;
      ConnectionParams &_params;
  };

}
