// Crow.cpp : Defines the entry point for the console application.
//

#include "common/views/CrowView.h"
#include "common/controllers/ListController.h"
#include "common/services/SettingsService.h"
#include "common/models/ListModel.h"
#include "common/services/MysqlAdapter.h"

using namespace std;

int main(int argc, char* argv[])
{
	if (argc < 2) {
		cerr << "Path to configDb.json should be passed as first parameter\n";
		return 0;
	}
	crow::SimpleApp app;
	SettingsService settings;
	auto params = settings.parseSettings(argv[1]);
	auto mysql_connection_factory = make_shared<MySQLConnectionFactory>(params.m_host,params.m_user,params.m_pass);
	auto mysql_pool = make_shared<ConnectionPool<MySQLConnection> >(100, mysql_connection_factory);

	CROW_ROUTE(app, "/hello")([]() {
		return "Hello world";
	});
	
	CROW_ROUTE(app, "/first_page_list")([&mysql_pool, &params]() {
		CrowView view;
		MysqlAdapter mysql(mysql_pool, params.m_name);
		ListModel model(mysql);
		ListController controller(model, view);
		controller.showFirstPageData();
		return view.getResult();
	});
	
	CROW_ROUTE(app, "/random_list")([&mysql_pool, &params]() {
		CrowView view;
		MysqlAdapter mysql(mysql_pool, params.m_name);
		ListModel model(mysql);
		ListController controller(model, view);
		controller.showRandomPageData();
		return view.getResult();
	});
	
	app.port(1238).multithreaded().run();
}

