'use strict';

class ListController {
    constructor(options) 
    {
        this.options = options;
    }
    
    showRandomData() 
    {
        this.options.model.getRandomData()
            .bind(this)
            .then(function (data) {
                this.options.view.renderJson(data);
            });
    }
    
    showFirstPageData() 
    {
        this.options.model.getFirstPageData()
            .bind(this)
            .then(function (data) {
                this.options.view.renderJson(data);
            });
    }
};

var create = function (options) {  
    return new ListController(options);
};

module.exports = {
    create: create
};