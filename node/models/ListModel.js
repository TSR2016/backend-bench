'use strict';

class ListModel
{
    constructor(options)
    {
        this.options = options;
    }
    
    getRandomInt(min, max) 
    {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    getRandomData() 
    {
        var idList = [];
        for(var i = 0; i < 50; i++) {
            idList.push(this.getRandomInt(1, 10000));
        }
        var sql = "SELECT * FROM bench_data WHERE id in (" + idList.join(',') + ")";
        return this.options.mysql.query(sql)
            .catch(function(e) {
                return {"error": e};
            });
    }
    
    getFirstPageData()
    {
        var sql = "SELECT * FROM bench_data WHERE id <= 50";
        return this.options.mysql.query(sql)
            .catch(function(e) {
                return {"error": e};
            });
    }
};

var create = function (options) {  
    return new ListModel(options);
};

module.exports = {
    create: create
};