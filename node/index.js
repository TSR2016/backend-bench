'use strict';

var cluster = require('cluster');
var http = require('http');
var express = require('express');
var app = express();

var port = 1235;

var ListController = require('./controllers/ListController');
var ListModel = require('./models/ListModel');
var ListView = require('./views/ListView');
var mysql = require('./services/MysqlAdapter');
var config = require('./../configDb.json');

mysql.create({
    'host': config.host,
    'user': config.user,
    'password': config.pass,
    'database': config.dbName
});
app.locals.mysql = mysql;
        
if (cluster.isMaster) {
    var cpuCount =  require('os').cpus().length
    for (var i = 0; i < cpuCount; i++) {
        cluster.fork();
    }

    cluster.on('exit', function(worker) {
        console.log('Worker &d died, replacing', worker.id);
        cluster.fork();
    })
} else {
    app.get('/hello',  function(req, res, next) {
        res.status(200).send('Hello');
    });
            
    app.get('/random_data',  function(req, res, next) {
        var model = ListModel.create({
            "mysql": req.app.locals.mysql
        });
        
        var view = ListView.create({
            "request": res
        });
        
        var controller = ListController.create({
            "model": model,
            "view": view
        });
        
        controller.showRandomData();
    });

    app.get('/first_page_data',  function(req, res, next) {
        var model = ListModel.create({
            "mysql": req.app.locals.mysql
        });
        
        var view = ListView.create({
            "request": res
        });
        
        var controller = ListController.create({
            "model": model,
            "view": view
        });
        
        controller.showFirstPageData();
    });
    
    http.createServer(app).listen(port, '0.0.0.0', function(){
        console.log('Express server listening on port ' + port);
        console.log('Port =  ' + port);
    });
}