'use strict';

class ListView
{
    constructor(options)
    {
        this.options = options;
    }
    
    renderJson(result) 
    {
        this.options.request.status(200).send(result);
    }
};

var create = function (options) {  
    return new ListView(options);
};

module.exports = {
    create: create
};