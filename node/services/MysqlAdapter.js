var mysql = require("mysql");
var Promise = require("bluebird");
var using = Promise.using;
Promise.promisifyAll(require("mysql/lib/Connection").prototype);
Promise.promisifyAll(require("mysql/lib/Pool").prototype);

var pool;

var create = function (config) {
    pool = mysql.createPool({
        connectionLimit: 100,
        host: config.host,
        user: config.user,
        password: config.password,
        database: config.database
    });   
}

var getConnection = function () {
    return pool.getConnectionAsync().disposer(function (connection) {
        return connection.destroy();
    });
};

var query = function (command) {
    return using(getConnection(), function (connection) {
        return connection.queryAsync(command);
    });
};

module.exports = {
    create: create,
    query: query
};